//
//  SceneDelegate.h
//  11
//
//  Created by syqb on 2020/6/15.
//  Copyright © 2020 syqb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

